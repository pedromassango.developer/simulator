import 'package:flutter/material.dart';
import 'package:injectorio/injectorio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simulator/src/data/proposals_repository.dart';
import 'package:simulator/src/data/competitors_repository.dart';
import 'package:simulator/src/data/database/app_database.dart';
import 'package:simulator/src/data/dummy_data.dart';
import 'package:simulator/src/data/industries_fee_repository.dart';
import 'package:simulator/src/data/industries_repository.dart';
import 'package:simulator/src/data/shared_preferences/session_preferences.dart';
import 'package:simulator/src/features/home_page.dart';

void main() async {

  final appDatabase = AppDatabase.getInstance();
  final sharedPreferences = await SharedPreferences.getInstance();

  final sessionPreferences = SessionPreferences(sharedPreferences);

  final competitorsRepository = CompetitorsRepository(appDatabase.competitorsDao);
  final industriesRepository = IndustriesRepository(appDatabase.industriesDao);
  final industriesFeeRepository = IndustriesFeeRepository(appDatabase.industriesFeeDao);

  InjectorIO.start()
      .factory(() => industriesRepository)
      .factory(() => competitorsRepository)
      .factory(() => industriesFeeRepository)
      .factory(() => ProposalsRepository(appDatabase.proposalsDao));

  // Seed data on first run
  if(sessionPreferences.isFirstRun()) {
    sessionPreferences.setFirstRun(false);
    await industriesRepository.insertAll(DummyData.industries);
    await competitorsRepository.insertAll(DummyData.competitors);
    await industriesFeeRepository.saveIndustriesFee(DummyData.industriesFee);
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simulator',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.orange
      ),
      home: HomePage(),
    );
  }
}
