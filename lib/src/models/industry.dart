import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'industry.g.dart';

@JsonSerializable(explicitToJson: true)
class Industry {
  final String id;
  final String name;

  Industry({@required this.id, @required this.name});

  factory Industry.fromJson(Map<String, dynamic> json) =>
    _$IndustryFromJson(json);

  Map<String, dynamic> toJson() => _$IndustryToJson(this);
}
