import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'company.g.dart';

@JsonSerializable()
class Company {
  final String id;
  final String name;

  Company({
    @required this.id,
    @required this.name,
  })  : assert(id != null),
        assert(name != null);

  factory Company.fromJson(Map<String, dynamic> json) =>
      _$CompanyFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyToJson(this);
}
