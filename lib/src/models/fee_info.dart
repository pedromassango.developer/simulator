import 'package:meta/meta.dart';
import 'package:simulator/src/models/fee_type.dart';

class FeeInfo {
  final FeeType type;
  final double competitorFee;
  final double proposedDiscount;

  FeeInfo({
    @required this.type,
    this.competitorFee = 0.0,
    this.proposedDiscount = 0.0,
  });

  bool get hasData => competitorFee > 0.0 && proposedDiscount > 0.0;

  FeeInfo copyWith({
    double competitorFee,
    double proposedDiscount,
    FeeType type,
  }) {
    return FeeInfo(
      type: type ?? this.type,
      proposedDiscount: proposedDiscount ?? this.proposedDiscount,
      competitorFee: competitorFee ?? this.competitorFee,
    );
  }

  double feeWithDiscount() {
    return competitorFee - proposedDiscount;
  }

  String feeWithDiscountAsString() => feeWithDiscount().toStringAsFixed(1);

  double discountPercentage() =>
      (competitorFee - (competitorFee * proposedDiscount) / 100);
}
