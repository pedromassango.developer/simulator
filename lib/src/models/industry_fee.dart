import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'industry_fee.g.dart';

@JsonSerializable()
class IndustryFee {
  final String industryId;
  final double minDebitFee;
  final double minCreditFee;

  IndustryFee({
    @required this.industryId,
    @required this.minDebitFee,
    @required this.minCreditFee,
  });

  bool isCreditFeeAllowed(double creditFee) => creditFee > minCreditFee;

  bool isDebitFeeAllowed(double debitFee) => debitFee > minDebitFee;


  factory IndustryFee.fromJson(Map<String, dynamic> json) =>
      _$IndustryFeeFromJson(json);

  Map<String, dynamic> toJson() => _$IndustryFeeToJson(this);
}
