
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'proposal.g.dart';

enum ProposalStatus { accepted, denied }

@JsonSerializable(explicitToJson: true)
class Proposal {
  final String id;

  final ProposalStatus status;

  final String competitorId;
  final String competitorName;
  final double competitorDebitFee;
  final double competitorCreditFee;

  final double proposedDebitFee;
  final double proposedCreditFee;

  final String industryName;

  final double debitDiscountFeePercentage;
  final double creditDiscountFeePercentage;

  final String clientCpfOrCnpj;
  final String clientCellphone;
  final String clientEmail;

  final DateTime acceptedAt;

  bool get wasAccepted => status == ProposalStatus.accepted;

  Proposal({
    @required this.id,
    @required this.status,
    @required this.competitorId,
    @required this.competitorName,
    @required this.competitorCreditFee,
    @required this.competitorDebitFee,
    @required this.proposedCreditFee,
    @required this.proposedDebitFee,
    @required this.debitDiscountFeePercentage,
    @required this.creditDiscountFeePercentage,
    @required this.industryName,
    @required this.acceptedAt,
    @required this.clientEmail,
    @required this.clientCellphone,
    @required this.clientCpfOrCnpj,
  });

  factory Proposal.fromJson(Map<String, dynamic> json) => _$ProposalFromJson(json);

  Map<String, dynamic> toJson() => _$ProposalToJson(this);
}