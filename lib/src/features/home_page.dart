import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simulator/src/features/proposals/proposals_page.dart';
import 'package:simulator/src/features/simulator/simulator_page.dart';
import 'package:simulator/src/ui/app_style.dart';

class HomePage extends StatelessWidget {
  void _navigateTo(BuildContext context, Widget page) {
    PageRoute route;
    if (Platform.isIOS) {
      route = CupertinoPageRoute(builder: (_) => page);
    } else {
      route = MaterialPageRoute(builder: (_) => page);
    }
    Navigator.push(context, route);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Simulação'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
          child: Column(
            children: <Widget>[
              SizedBox.fromSize(
                size: Size(double.infinity, 45),
                child: MaterialButton(
                  minWidth: double.infinity,
                  child: Text(
                    'Nova Simulação',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: AppColors.buttonBackgroundColor,
                  onPressed: () => _navigateTo(context, SimulatorPage()),
                ),
              ),
              const SizedBox(height: 16),
              SizedBox.fromSize(
                size: Size(double.infinity, 45),
                child: MaterialButton(
                  minWidth: double.infinity,
                  child: Text('Visualizar propostas aceitas'),
                  shape: BeveledRectangleBorder(side: BorderSide(width: 0.5)),
                  onPressed: () =>
                      _navigateTo(context, ProposalsPage()),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
