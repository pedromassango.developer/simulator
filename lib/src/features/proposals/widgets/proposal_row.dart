import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:simulator/src/models/proposal.dart';
import 'package:simulator/src/ui/common/table_text_row.dart';

class ProposalRow implements TableRow {
  final Proposal proposal;

  ProposalRow({
    @required this.proposal,
  }) : assert(proposal != null);

  String _formatCreditFeeRow() {
    return proposal.proposedCreditFee.toString() +
        '(${proposal.creditDiscountFeePercentage}%)';
  }

  String _formatDebitFeeRow() {
    return proposal.proposedDebitFee.toString() +
        '(${proposal.debitDiscountFeePercentage}%)';
  }

  @override
  List<Widget> get children => [
    TableTextRow(DateFormat.yMd().format(proposal.acceptedAt)),
    TableTextRow(proposal.clientEmail),
    TableTextRow(proposal.clientCellphone),
    TableTextRow(proposal.clientCpfOrCnpj),
    TableTextRow(proposal.industryName),
    TableTextRow(proposal.competitorName),
    TableTextRow('${proposal.competitorCreditFee}%', Colors.red),
    TableTextRow('${proposal.competitorDebitFee}%', Colors.red),
    TableTextRow(_formatCreditFeeRow(), Colors.green),
    TableTextRow(_formatDebitFeeRow(), Colors.green),
    Icon(
      proposal.wasAccepted ? Icons.check_circle : Icons.cancel,
      color: proposal.wasAccepted ? Colors.green : Colors.red,
    )
  ];

  @override
  Decoration get decoration => BoxDecoration(
    border: Border(
      bottom: BorderSide(color: Colors.grey),
    ),
  );

  @override
  LocalKey get key => ObjectKey(proposal);
}