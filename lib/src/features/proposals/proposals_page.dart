import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:injectorio/injectorio.dart';
import 'package:simulator/src/data/proposals_repository.dart';
import 'package:simulator/src/features/proposals/store/proposals_store.dart';
import 'package:simulator/src/features/proposals/widgets/proposal_row.dart';
import 'package:simulator/src/models/proposal.dart';
import 'package:simulator/src/ui/common/table_header_text.dart';

class ProposalsPage extends StatefulWidget {
  @override
  _ProposalsPageState createState() => _ProposalsPageState();
}

class _ProposalsPageState extends State<ProposalsPage> {
  final ProposalsStore _proposalsStore = ProposalsStore(
      get<ProposalsRepository>());

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight]);

    _proposalsStore.loadAcceptedProposals();
  }

  @override
  void dispose() {
    _proposalsStore.dispose();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Propostas")),
      body: Observer(
        builder: (_) {
          if(_proposalsStore.isLoading)
            return Center(child: CircularProgressIndicator());

          if(!_proposalsStore.hasData)
            return Center(child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.error, size: 80, color: Colors.grey),
                Text("Sem propostas na base de dados"),
              ],
            ));

          return Stack(
            children: <Widget>[
              SingleChildScrollView(
                primary: true,
                child: SingleChildScrollView(
                  primary: false,
                  scrollDirection: Axis.horizontal,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: SizedBox(
                      width: 900,
                        child: _ProposalTable(acceptedProposals: _proposalsStore.proposals)),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class _ProposalTable extends StatelessWidget {
  final List<Proposal> acceptedProposals;

  const _ProposalTable({Key key, @required this.acceptedProposals})
      : assert(acceptedProposals != null);

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder(
        left: BorderSide(),
        bottom: BorderSide(),
        right: BorderSide(),
      ),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(color: Colors.grey),
            ),
          ),
          children: <Widget>[
            TableTitleText('Data'),
            TableTitleText('Email do Cliente'),
            TableTitleText('Telefone Cliente'),
            TableTitleText('CPF Cliente'),
            TableTitleText('Ramo de Atividade'),
            TableTitleText('Concorrente'),
            TableTitleText('Crédito'),
            TableTitleText('Dédito'),
            TableTitleText('Desc. Crédito'),
            TableTitleText('Desc. Dédito'),
            TableTitleText('Estado'),
          ],
        ),
        ...acceptedProposals.map((proposal) =>
            ProposalRow(proposal: proposal)).toList(),
      ],
    );
  }
}

