import 'package:mobx/mobx.dart';
import 'package:simulator/src/data/proposals_repository.dart';
import 'package:simulator/src/models/proposal.dart';

part 'proposals_store.g.dart';

class ProposalsStore = _ProposalsStore with _$ProposalsStore;

abstract class _ProposalsStore with Store {
  final ProposalsRepository _proposalsRepository;

  _ProposalsStore(this._proposalsRepository);

  @observable
  List<Proposal> proposals = [];

  @observable
  bool isLoading = false;

  @computed
  bool get hasData => proposals.isNotEmpty;

  @action
  loadAcceptedProposals() async {
    this.isLoading = true;

    final result = await _proposalsRepository.getAllProposals();

    this.isLoading = false;

    if(result != null && result.isNotEmpty) {
      proposals = result;
    }
  }
}
