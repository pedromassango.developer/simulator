import 'package:mobx/mobx.dart';
import 'package:simulator/src/data/competitors_repository.dart';
import 'package:simulator/src/data/industries_fee_repository.dart';
import 'package:simulator/src/data/industries_repository.dart';
import 'package:simulator/src/data/proposals_repository.dart';
import 'package:simulator/src/models/company.dart';
import 'package:simulator/src/models/fee_info.dart';
import 'package:simulator/src/models/fee_type.dart';
import 'package:simulator/src/models/industry.dart';
import 'package:simulator/src/models/industry_fee.dart';
import 'package:simulator/src/models/proposal.dart';

part 'simulator_store.g.dart';

class SimulatorStore = _SimulatorStoreBase with _$SimulatorStore;

abstract class _SimulatorStoreBase with Store {
  final _FeeData feeData;
  final _ClientData clientData;
  final _SimulationResults simulationResults;

  final IndustriesRepository _industriesRepository;
  final CompetitorsRepository _competitorsRepository;
  final IndustriesFeeRepository _industriesFeeRepository;
  final ProposalsRepository _acceptedProposalsRepository;

  _SimulatorStoreBase(
    this._industriesRepository,
    this._competitorsRepository,
    this._industriesFeeRepository,
      this._acceptedProposalsRepository,
  )   : feeData = _FeeData(_competitorsRepository),
        clientData = _ClientData(_industriesRepository),
        simulationResults = _SimulationResults();

  @observable
  bool isProcessingSimulation = false;

  @observable
  bool isProposalSaved = false;

  bool isProposalAccepted = false;

  @action
  startSimulation() async {
    isProcessingSimulation = true;

    final industry = clientData.industry;
    final debitFee = feeData.debitFee;
    final creditFee = feeData.creditFee;

    final industryFee = await _industriesFeeRepository.getIndustryFee(industry.id);

    final debitDiscountPercentage = debitFee.discountPercentage();
    final creditDiscountPercentage = creditFee.discountPercentage();

    simulationResults.industryFee = industryFee;
    simulationResults.isDebitFeeAllowed = industryFee.isDebitFeeAllowed(debitDiscountPercentage);
    simulationResults.isCreditFeeAllowed = industryFee.isCreditFeeAllowed(creditDiscountPercentage);
    simulationResults.debitDiscountFeePercentage = debitDiscountPercentage;
    simulationResults.creditDiscountFeePercentage = creditDiscountPercentage;

    isProcessingSimulation = false;
  }

  @action
  proposalAccepted() async {
    await _saveProposal(ProposalStatus.accepted);
    this.isProposalAccepted = true;
    this.isProposalSaved = true;
  }

  _saveProposal(ProposalStatus status) async {
    final currentTime = DateTime.now();

    final proposal = Proposal(
      id: currentTime.toIso8601String(),
      acceptedAt: currentTime,
      status: status,
      industryName: clientData.industry.name,
      competitorId: feeData.competitor.id,
      competitorName: feeData.competitor.name,
      clientCellphone: clientData.cellphone,
      clientEmail: clientData.email,
      clientCpfOrCnpj: clientData.cpf,
      competitorDebitFee: feeData.debitFee.competitorFee,
      competitorCreditFee: feeData.creditFee.competitorFee,
      proposedCreditFee: feeData.creditFee.proposedDiscount,
      proposedDebitFee: feeData.debitFee.proposedDiscount,
      debitDiscountFeePercentage: simulationResults.debitDiscountFeePercentage,
      creditDiscountFeePercentage: simulationResults.creditDiscountFeePercentage,
    );

    await _acceptedProposalsRepository.saveProposal(proposal);
  }

  @action
  proposalDenied() async {
    await _saveProposal(ProposalStatus.denied);
    this.isProposalAccepted = false;
    this.isProposalSaved = true;
  }
}

class _ClientData = _ClientDataBase with _$_ClientData;

abstract class _ClientDataBase with Store {
  final IndustriesRepository _industriesRepository;

  _ClientDataBase(this._industriesRepository)
      : assert(_industriesRepository != null);

  @observable
  List<Industry> industries = [];

  @observable
  String cpf = '';

  @observable
  String cpfError;

  @observable
  String cellphone = '';

  @observable
  String cellphoneError;

  @observable
  String email = '';

  @observable
  String emailError;

  @observable
  Industry industry;

  @computed
  bool get isFormFilled =>
      cpfError == null &&
      cpf.isNotEmpty &&
      cellphoneError == null &&
      cellphone.isNotEmpty &&
      emailError == null &&
      industry != null;

  @action
  void setCpf(String cpf) {
    this.cpf = cpf;

    if (cpf.trim().length < 11 || cpf.trim().length > 11) {
      cpfError = "CNPJ ou CPF inválido";
      return;
    }

    cpfError = null;
  }

  @action
  void setCellphone(String cellphone) {
    this.cellphone = cellphone;

    if (cellphone.trim().isEmpty || cellphone.length < 10) {
      cellphoneError = "Número inválido";
      return;
    }

    cellphoneError = null;
  }

  @action
  void setEmail(String email) {
    this.email = email;

    if (email.isNotEmpty && !email.contains('@')) {
      emailError = 'Email inválido';
      return;
    }

    emailError = null;
  }

  @action
  void selectIndustry(String industryName) {
    final selectedIndustry =
        industries.firstWhere((e) => e.name.startsWith(industryName));

    this.industry = selectedIndustry;
  }

  @action
  void loadIndustries() {
    _industriesRepository.getAllIndustries().then((result) {
      if (result != null) {
        industries = result;
      }
    });
  }
}

class _FeeData = _FeeDataBase with _$_FeeData;

abstract class _FeeDataBase with Store {
  final CompetitorsRepository _competitorsRepository;

  _FeeDataBase(this._competitorsRepository);

  @observable
  List<Company> competitors = [];

  @observable
  Company competitor;

  @observable
  FeeInfo debitFee = FeeInfo(type: FeeType.debit);

  @observable
  FeeInfo creditFee = FeeInfo(type: FeeType.credit);

  @computed
  bool get isFormFilled =>
      competitor != null && debitFee.hasData && creditFee.hasData;

  bool _isValidNumber(String value) => double.tryParse(value) != null;

  @action
  void setCompetitor(String competitorName) {
    final selectedCompetitor = competitors.firstWhere((competitor) {
      return competitor.name.startsWith(competitorName);
    });

    this.competitor = selectedCompetitor;
  }

  @action
  void setCompetitorFee(FeeType type, String value) {
    if (!_isValidNumber(value)) {
      return;
    }
    final fee = double.parse(value);
    _updateCompetitorFee(type, fee);
  }

  @action
  void setProposedFee(FeeType type, String value) {
    if (!_isValidNumber(value)) {
      return;
    }
    final fee = double.parse(value);
    _updateProposedDiscount(type, fee);
  }

  @action
  void loadCompetitors() {
    _competitorsRepository.getCompetitors().then((result) {
      if (result != null) {
        competitors = result;
      }
    });
  }

  void _updateCompetitorFee(FeeType type, double fee) {
    switch (type) {
      case FeeType.credit:
        creditFee = creditFee.copyWith(competitorFee: fee);
        break;
      case FeeType.debit:
        debitFee = debitFee.copyWith(competitorFee: fee);
        break;
    }
  }

  void _updateProposedDiscount(FeeType type, double fee) {
    switch (type) {
      case FeeType.credit:
        creditFee = creditFee.copyWith(proposedDiscount: fee);
        break;
      case FeeType.debit:
        debitFee = debitFee.copyWith(proposedDiscount: fee);
        break;
    }
  }
}

class _SimulationResults = _SimulationResultsBase with _$_SimulationResults;

abstract class _SimulationResultsBase with Store {

  bool isDebitFeeAllowed = false;

  bool isCreditFeeAllowed = false;

  IndustryFee industryFee;

  double debitDiscountFeePercentage;
  double creditDiscountFeePercentage;

  bool get isSimulationAllowed => isDebitFeeAllowed && isCreditFeeAllowed;
}
