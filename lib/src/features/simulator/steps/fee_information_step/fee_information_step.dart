import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:simulator/src/features/simulator/steps/fee_information_step/form_container.dart';
import 'package:simulator/src/features/simulator/store/simulator_store.dart';

class FeeInformationStep extends StatelessWidget {
  final VoidCallback onStartSimulationPressed;
  final SimulatorStore simulatorStore;

  const FeeInformationStep({
    @required this.onStartSimulationPressed,
    @required this.simulatorStore,
  })
      : assert(onStartSimulationPressed != null),
        assert(simulatorStore != null);

  @override
  Widget build(BuildContext context) {
    final feeData = simulatorStore.feeData;

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Informações de taxas',
                style: Theme.of(context).textTheme.headline,
              ),
              Text(
                'Os campos obrigatórios estão sinalizados com *',
                style: Theme.of(context).textTheme.subhead.copyWith(color: Colors.grey)
              ),
              Padding(
                padding: const EdgeInsets.only(top: 32),
                child: FormContainer(simulatorStore: simulatorStore),
              ),
            ],
          ),
        ),
        const Spacer(),
        SizedBox.fromSize(
          size: Size(double.infinity, 45),
          child: Observer(
            builder: (_) => MaterialButton(
              color: Theme.of(context).accentColor,
              disabledColor: Colors.grey,
              child: simulatorStore.isProcessingSimulation ? CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ) : Text('Simular', style: TextStyle(color: Colors.white)),
              onPressed: feeData.isFormFilled ? onStartSimulationPressed : null,
            ),
          ),
        )
      ],
    );
  }
}
