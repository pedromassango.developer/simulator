import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:simulator/src/features/simulator/steps/widgets/bordered_dropdown_field.dart';
import 'package:simulator/src/features/simulator/steps/widgets/bordered_input_field.dart';
import 'package:simulator/src/features/simulator/store/simulator_store.dart';
import 'package:simulator/src/models/fee_type.dart';

class FormContainer extends StatelessWidget {
  final SimulatorStore simulatorStore;

  const FormContainer({
    @required this.simulatorStore,
  }) : assert(simulatorStore != null);

  @override
  Widget build(BuildContext context) {
    final feeData = simulatorStore.feeData;

    return Container(
      height: 350,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Observer(builder: (_) =>
              BorderedDropdownField(
                headerText: 'Concorrente *',
                items: feeData.competitors.map((competitor) {
                  return competitor.name;
                }).toList(),
                onChanged: feeData.setCompetitor,
              ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 8),
            child: Text(
              'Débito',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          Row(
            children: <Widget>[
              Flexible(
                child: BorderedInputField.number(
                  labelText: 'Taxa do concorrente *',
                  errorText: null,
                  onChanged: (value) => feeData.setCompetitorFee(FeeType.debit, value),
                ),
              ),
              const SizedBox(width: 12),
              Flexible(
                child: BorderedInputField.number(
                  labelText: 'Desconto oferecido *',
                  errorText: null,
                  onChanged: (value) => feeData.setProposedFee(FeeType.debit, value),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 8),
            child: Text(
              'Crédito',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          Row(
            children: <Widget>[
              Flexible(
                child: BorderedInputField.number(
                  labelText: 'Taxa do concorrente *',
                  errorText: null,
                  onChanged: (value) => feeData.setCompetitorFee(FeeType.credit, value),
                ),
              ),
              const SizedBox(width: 12),
              Flexible(
                child: BorderedInputField.number(
                  labelText: 'Desconto oferecido *',
                  errorText: null,
                  onChanged: (value) => feeData.setProposedFee(FeeType.credit, value),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
