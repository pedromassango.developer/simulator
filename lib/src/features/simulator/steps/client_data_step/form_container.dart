import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:simulator/src/features/simulator/steps/widgets/bordered_dropdown_field.dart';
import 'package:simulator/src/features/simulator/steps/widgets/bordered_input_field.dart';
import 'package:simulator/src/features/simulator/store/simulator_store.dart';

class FormContainer extends StatelessWidget {
  final SimulatorStore simulatorStore;

  const FormContainer({
    Key key,
    @required this.simulatorStore,
  }) : assert(simulatorStore != null);

  @override
  Widget build(BuildContext context) {
    final clientData = simulatorStore.clientData;

    return Container(
      height: 300,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Flexible(
                child: Observer(
                  builder: (_) => BorderedInputField(
                    labelText: 'CNPJ ou CPF *',
                    errorText: clientData.cpfError,
                    inputType: TextInputType.number,
                    onChanged: clientData.setCpf,
                  ),
                ),
              ),
              const SizedBox(width: 16),
              Flexible(
                child: Observer(
                  builder: (_) => BorderedInputField(
                    labelText: 'Telefone *',
                    inputType: TextInputType.phone,
                    errorText: clientData.cellphoneError,
                    onChanged: clientData.setCellphone,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Observer(
              builder: (_) => BorderedInputField(
                labelText: 'E-mail',
                inputType: TextInputType.emailAddress,
                errorText: clientData.emailError,
                onChanged: clientData.setEmail,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: Observer(
              builder: (_) => BorderedDropdownField(
                headerText: 'Ramo de Actividade *',
                items: clientData.industries
                    .map((industry) => industry.name)
                    .toList(),
                onChanged: clientData.selectIndustry,
              ),
            ),
          )
        ],
      ),
    );
  }
}