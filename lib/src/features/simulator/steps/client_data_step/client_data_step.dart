import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:simulator/src/features/simulator/steps/client_data_step/form_container.dart';
import 'package:simulator/src/features/simulator/store/simulator_store.dart';

class ClientDataStep extends StatelessWidget {
  final VoidCallback onNextPressed;
  final SimulatorStore simulatorStore;

  const ClientDataStep({
    Key key,
    @required this.simulatorStore,
    @required this.onNextPressed,
  })  : assert(simulatorStore != null),
        assert(onNextPressed != null);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Dados do cliente',
                style: Theme.of(context).textTheme.headline,
              ),
              Text(
                'Os campos obrigatórios estão sinalizados com *',
                style: Theme.of(context)
                    .textTheme.subhead.copyWith(color: Colors.grey),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 32),
                child: FormContainer(simulatorStore: simulatorStore),
              ),
            ],
          ),
        ),
        const Spacer(),
        SizedBox.fromSize(
          size: Size(double.infinity, 45),
          child: Observer(
            builder: (_) => MaterialButton(
              color: Theme.of(context).accentColor,
              disabledColor: Colors.grey,
              child: Text('Próximo', style: TextStyle(color: Colors.white)),
              onPressed: simulatorStore.clientData.isFormFilled ? onNextPressed : null,
            ),
          ),
        )
      ],
    );
  }
}
