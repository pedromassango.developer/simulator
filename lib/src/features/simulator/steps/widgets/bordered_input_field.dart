import 'package:flutter/material.dart';

class BorderedInputField extends StatelessWidget {
  final String labelText;
  final String errorText;
  final TextInputType inputType;
  final Function(String) onChanged;

  const BorderedInputField({
    Key key,
    @required this.labelText,
    this.errorText,
    this.onChanged,
    this.inputType = TextInputType.text,
  })  : assert(labelText != null),
        assert(inputType != null),
        super(key: key);

  factory BorderedInputField.number({
    @required String labelText,
    @required String errorText,
    Function(String) onChanged,
  }) {
    return BorderedInputField(
      labelText: labelText,
      errorText: errorText,
      onChanged: onChanged,
      inputType: TextInputType.numberWithOptions(decimal: true),
    );
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: inputType,
      onChanged: onChanged,
      decoration: InputDecoration(
        labelText: labelText,
        errorText: errorText,
        labelStyle: TextStyle(fontSize: 14),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.zero,
        ),
      ),
    );
  }
}
