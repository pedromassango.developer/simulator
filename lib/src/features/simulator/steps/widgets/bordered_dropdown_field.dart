import 'package:flutter/material.dart';

class BorderedDropdownField extends StatefulWidget {
  final String headerText;
  final List<String> items;
  final Function(String) onChanged;

  BorderedDropdownField({Key key,
    @required this.headerText,
    @required this.items,
    @required this.onChanged,
  })
      : assert(headerText != null),
        assert(items != null),
        assert(onChanged != null),
        super(key: key);

  @override
  _BorderedDropdownFieldState createState() => _BorderedDropdownFieldState();
}

class _BorderedDropdownFieldState extends State<BorderedDropdownField> {
  String selectedValue;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Text(widget.headerText,
            style: Theme.of(context).textTheme.subhead.copyWith(
                color: Colors.grey
            ),
          ),
        ),
        DropdownButtonFormField<String>(
          hint: Text("Selecione"),
          value: selectedValue,
          onChanged: (value) {
            setState(() => selectedValue = value);
            widget.onChanged(value);
          },
          items: widget.items.map((item) {
            return DropdownMenuItem<String>(
              child: Text(item, style: TextStyle(color: Colors.black),),
              value: item,
            );
          }).toList(),
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.zero,
              )
          ),
        ),
      ],
    );
  }
}