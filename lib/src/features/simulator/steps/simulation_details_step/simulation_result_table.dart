import 'package:flutter/material.dart';
import 'package:simulator/src/features/simulator/store/simulator_store.dart';
import 'package:simulator/src/models/fee_info.dart';
import 'package:simulator/src/models/fee_type.dart';
import 'package:simulator/src/ui/common/table_header_text.dart';
import 'package:simulator/src/ui/common/table_text_row.dart';

class SimulationResultTable extends StatelessWidget {
  final SimulatorStore simulatorStore;

  const SimulationResultTable({Key key, @required this.simulatorStore})
      : assert(simulatorStore != null);

  @override
  Widget build(BuildContext context) {
    final simulationResults = simulatorStore.simulationResults;
    final feeData = simulatorStore.feeData;

    return Table(
      children: <TableRow>[
        TableRow(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.grey),
              ),
            ),
            children: <Widget>[
              TableTitleText('Tipo'),
              TableTitleText('Concorrente'),
              TableTitleText('Proposta'),
            ]),
        _FeeTableRow(
          fee: feeData.debitFee,
          proposalFeeRowColor: simulationResults.isDebitFeeAllowed
              ? Colors.green
              : Colors.red,
        ),
        _FeeTableRow(
          fee: feeData.creditFee,
          proposalFeeRowColor: simulationResults.isCreditFeeAllowed
              ? Colors.green
              : Colors.red,
        ),
      ],
    );
  }
}

class _FeeTableRow implements TableRow {
  final FeeInfo fee;
  final Color proposalFeeRowColor;

  _FeeTableRow({
    @required this.fee,
    @required this.proposalFeeRowColor,
  }) : assert(fee != null),
        assert(proposalFeeRowColor != null);

  String _getTaxTypeName(FeeType taxType) {
    return taxType == FeeType.credit ? 'Crédito' : 'Débito';
  }

  @override
  List<Widget> get children => [
    TableTextRow(_getTaxTypeName(fee.type)),
    TableTextRow(fee.competitorFee.toString() + '%'),
    TableTextRow(fee.discountPercentage().toString() + '%',
        proposalFeeRowColor),
  ];

  @override
  Decoration get decoration => BoxDecoration(
    border: Border(
      bottom: BorderSide(color: Colors.grey),
    ),
  );

  @override
  LocalKey get key => ObjectKey(fee);
}