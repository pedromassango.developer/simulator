import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mobx/mobx.dart';
import 'package:simulator/src/features/simulator/steps/simulation_details_step/simulation_result_table.dart';
import 'package:simulator/src/features/simulator/store/simulator_store.dart';
import 'package:simulator/src/ui/app_style.dart';

class SimulationStepDetailsStep extends StatefulWidget {
  final SimulatorStore simulatorStore;

  SimulationStepDetailsStep({@required this.simulatorStore})
      : assert(simulatorStore != null);

  @override
  _SimulationStepDetailsStepState createState() => _SimulationStepDetailsStepState();
}

class _SimulationStepDetailsStepState extends State<SimulationStepDetailsStep> {

  final List<ReactionDisposer> _disposers = [];

  void _showProposalSaved() {
    final title = widget.simulatorStore.isProposalAccepted ?
        'Aceite do Cliente' : 'Proposta negada';

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) {
        return AlertDialog(
          title: Text(title),
          content: Text("Resposta gravada no banco de dados!"),
          actions: <Widget>[
            FlatButton(
              child: Text("Voltar para inicio"),
              onPressed: () => Navigator.popUntil(context, (r) => r.isFirst),
            )
          ],
        );
      }
    );
  }
  
  @override
  void dispose() {
    _disposers.forEach((disposer) => disposer());
    super.dispose();
  }
  
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _disposers.add(
        autorun((_) {
          if(widget.simulatorStore.isProposalSaved) {
            _showProposalSaved();
          }
        })
    );
  }

  @override
  Widget build(BuildContext context) {
    final simulationResults = widget.simulatorStore.simulationResults;
    final industryFee = simulationResults.industryFee;

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
          child: Column(
            children: <Widget>[
              Text(
                simulationResults.isSimulationAllowed ?
                'Taxas da simulação permitidas' :
                'Taxas da simulação não permitidas',
                style: Theme.of(context).textTheme.headline,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4, bottom: 32),
                child: Text(
                  simulationResults.isSimulationAllowed ?
                  'Todas as taxas simuladas são permitidas. Ofereça a seu cliente.' :
                  'A(s) taxa(s) simulada(s) não foi(ram) permitidas(s) pois não atinge(m) o mínimo requisitado.',
                  style: Theme.of(context)
                      .textTheme.subhead.copyWith(color: Colors.grey),
                ),
              ),
              SimulationResultTable(simulatorStore: widget.simulatorStore),
              Visibility(
                visible: !simulationResults.isSimulationAllowed,
                child: Padding(
                  padding: const EdgeInsets.only(top: 32),
                  child: Text(
                      'O valor de taxa mínima para débito é ${industryFee
                          .minDebitFee} enquanto para crédito é ${industryFee
                          .minCreditFee}',
                    style: Theme.of(context).textTheme.subhead.copyWith(
                        color: Colors.grey
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        const Spacer(),
        Visibility(
          visible: simulationResults.isSimulationAllowed,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox.fromSize(
                size: Size(double.infinity, 45),
                child: MaterialButton(
                  color: AppColors.buttonBackgroundColor,
                  child: Text("Proposta aceita", style: TextStyle(color: Colors.white)),
                  onPressed: () => widget.simulatorStore.proposalAccepted(),
                ),
              ),
              SizedBox.fromSize(
                size: Size(double.infinity, 45),
                child: MaterialButton(
                  child: Text("Recusar", style: TextStyle(color: Colors.grey)),
                  onPressed: () => widget.simulatorStore.proposalDenied(),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
