import 'package:flutter/material.dart';
import 'package:injectorio/injectorio.dart';
import 'package:simulator/src/data/proposals_repository.dart';
import 'package:simulator/src/data/competitors_repository.dart';
import 'package:simulator/src/data/industries_fee_repository.dart';
import 'package:simulator/src/data/industries_repository.dart';
import 'package:simulator/src/features/simulator/store/simulator_store.dart';
import 'package:simulator/src/features/simulator/steps/client_data_step/client_data_step.dart';
import 'package:simulator/src/features/simulator/steps/simulation_details_step/simulation_details_step.dart';
import 'package:simulator/src/features/simulator/steps/fee_information_step/fee_information_step.dart';

class SimulatorPage extends StatefulWidget {
  @override
  _SimulatorPageState createState() => _SimulatorPageState();
}

class _SimulatorPageState extends State<SimulatorPage> {
  final Duration _pageTransitionDuration = Duration(milliseconds: 270);
  PageController _pageController;

  SimulatorStore _simulatorStore;

  void _moveToStep(int page) {
    _pageController.animateToPage(
      page,
      duration: _pageTransitionDuration,
      curve: Curves.linear,
    );
  }

  void _moveToNextStep() {
    _moveToStep(_pageController.page.toInt() + 1);
  }

  void _onBackPressed(BuildContext context) {
    print(_pageController.page.round());
    if (_pageController.page.round() != 0) {
      final page = _pageController.page.toInt() - 1;
      _moveToStep(page);
    } else {
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    super.initState();

    _simulatorStore = SimulatorStore(
      get<IndustriesRepository>(),
      get<CompetitorsRepository>(),
      get<IndustriesFeeRepository>(),
      get<ProposalsRepository>(),
    );

    _simulatorStore.clientData.loadIndustries();
    _simulatorStore.feeData.loadCompetitors();

    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _simulatorStore.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Simulador'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => _onBackPressed(context),
        ),
      ),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height - 56.0,
        child: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            ClientDataStep(
              simulatorStore: _simulatorStore,
              onNextPressed: _moveToNextStep,
            ),
            FeeInformationStep(
              simulatorStore: _simulatorStore,
              onStartSimulationPressed: () async {
                await _simulatorStore.startSimulation();
                _moveToNextStep();
              },
            ),
            SimulationStepDetailsStep(simulatorStore: _simulatorStore)
          ],
        ),
      ),
    );
  }
}
