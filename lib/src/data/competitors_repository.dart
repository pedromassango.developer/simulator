
import 'package:simulator/src/data/database/daos/competitors_dao.dart';
import 'package:simulator/src/models/company.dart';

class CompetitorsRepository {
  final CompetitorsDao _competitorsDao;

  CompetitorsRepository(this._competitorsDao) : assert(_competitorsDao != null);

  Future<void> insertAll(List<Company> competitors) async {
    await _competitorsDao.insertCompanies(competitors);
  }

  Future<List<Company>> getCompetitors() async {
    return await _competitorsDao.getAllCompanies();
  }
}