import 'package:shared_preferences/shared_preferences.dart';

class SessionPreferences {
  final String _firstRunKey = "_firstRunKey";

  final SharedPreferences _preferences;

  SessionPreferences(this._preferences) : assert(_preferences != null);

  bool isFirstRun() => _preferences.getBool(_firstRunKey) ?? true;

  Future setFirstRun(bool value) async {
    await _preferences.setBool(_firstRunKey, value);
  }
}