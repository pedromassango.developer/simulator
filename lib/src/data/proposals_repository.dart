import 'package:simulator/src/data/database/daos/proposals_dao.dart';
import 'package:simulator/src/models/proposal.dart';

class ProposalsRepository {
  final ProposalsDao _proposalsDao;

  ProposalsRepository(this._proposalsDao)
      : assert(_proposalsDao != null);

  Future<void> saveProposal(Proposal proposal) async {
    await _proposalsDao.insertProposal(proposal);
  }

  Future<List<Proposal>> getAllProposals() async {
    final data = await _proposalsDao.getAllAcceptedProposals();
    return data ?? [];
  }
}
