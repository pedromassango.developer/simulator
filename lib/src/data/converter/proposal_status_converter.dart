import 'package:simulator/src/models/enum_values.dart';
import 'package:simulator/src/models/proposal.dart';

class ProposalStatusConverter {
  static final _values = EnumValues<ProposalStatus>({
    'accepted': ProposalStatus.accepted,
    'denied': ProposalStatus.denied,
  });

  static String encode(ProposalStatus status) => _values.reverse[status];

  static ProposalStatus decode(String status) => _values.map[status];
}
