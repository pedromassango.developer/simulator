import 'package:simulator/src/data/database/daos/industries_dao.dart';
import 'package:simulator/src/models/industry.dart';

class IndustriesRepository {
  final IndustriesDao _industriesDao;

  IndustriesRepository(this._industriesDao) : assert(_industriesDao != null);

  Future<List<Industry>> getAllIndustries() async {
    return await _industriesDao.getAllIndustries();
  }

  Future<void> insertAll(List<Industry> industries) async {
    await _industriesDao.insertIndustries(industries);
  }
}
