import 'package:simulator/src/models/company.dart';
import 'package:simulator/src/models/industry.dart';
import 'package:simulator/src/models/industry_fee.dart';

class DummyData {
  static final industries = <Industry>[
    Industry(id: '1', name: 'Eletrônicos'),
    Industry(id: '2', name: 'Eletrodomésticos'),
    Industry(id: '3', name: 'Compras Coletivas'),
    Industry(id: '4', name: 'Cursos e Educação'),
    Industry(id: '5', name: 'Cosméticos e Perfumaria'),
    Industry(id: '6', name: 'Supermercado'),
  ];

  static final industriesFee = <IndustryFee>[
    IndustryFee(industryId: '1', minCreditFee: 3.5, minDebitFee: 2.0),
    IndustryFee(industryId: '2', minCreditFee: 2.8, minDebitFee: 1.9),
    IndustryFee(industryId: '3', minCreditFee: 3.8, minDebitFee: 1.5),
    IndustryFee(industryId: '4', minCreditFee: 2.5, minDebitFee: 2.1),
    IndustryFee(industryId: '5', minCreditFee: 2.3, minDebitFee: 1.7),
    IndustryFee(industryId: '6', minCreditFee: 3.0, minDebitFee: 2.0),
  ];

  static final competitors = <Company>[
    Company(id: '1', name: 'Empresa 1'),
    Company(id: '2', name: 'Empresa 2'),
    Company(id: '3', name: 'Empresa 3'),
    Company(id: '4', name: 'Empresa 4'),
    Company(id: '5', name: 'Empresa 5'),
  ];
}
