
import 'package:simulator/src/data/database/daos/industries_fee_dao.dart';
import 'package:simulator/src/models/industry_fee.dart';

class IndustriesFeeRepository {
  final IndustriesFeeDao _industriesFeeDao;

  IndustriesFeeRepository(this._industriesFeeDao);

  Future<void> saveIndustriesFee(List<IndustryFee> industriesFee) async {
    await _industriesFeeDao.insertIndustriesFee(industriesFee);
  }

  Future<IndustryFee> getIndustryFee(String industryId) async {
    return _industriesFeeDao.getIndustryFee(industryId);
  }
}