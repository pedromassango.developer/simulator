import 'package:moor/moor.dart';
import 'package:simulator/src/data/converter/proposal_status_converter.dart';
import 'package:simulator/src/data/database/app_database.dart';
import 'package:simulator/src/data/database/tables/proposals_table.dart';
import 'package:simulator/src/models/proposal.dart';

part 'proposals_dao.g.dart';

@UseDao(tables: [ProposalsTable])
class ProposalsDao extends DatabaseAccessor<AppDatabase>
    with _$ProposalsDaoMixin {
  ProposalsDao(AppDatabase db) : super(db);

  Future<void> insertProposal(Proposal proposal) async {
    final data = DbProposal(
      id: proposal.id,
      status: ProposalStatusConverter.encode(proposal.status),
      acceptedAt: proposal.acceptedAt,
      competitorId: proposal.competitorId,
      competitorName: proposal.competitorName,
      proposedDebitFee: proposal.proposedDebitFee,
      proposedCreditFee: proposal.proposedCreditFee,
      competitorDebitFee: proposal.competitorDebitFee,
      competitorCreditFee: proposal.competitorCreditFee,
      creditDiscountFeePercentage: proposal.creditDiscountFeePercentage,
      debitDiscountFeePercentage: proposal.debitDiscountFeePercentage,
      industryName: proposal.industryName,
      clientCellphone: proposal.clientCellphone,
      clientCpfOrCnpj: proposal.clientCpfOrCnpj,
      clientEmail: proposal.clientEmail
    );

    await into(proposalsTable).insert(data);
  }

  Future<List<Proposal>> getAllAcceptedProposals() async {
    Proposal convertDataModel(DbProposal data) {
      return Proposal(
        id: data.id,
        status: ProposalStatusConverter.decode(data.status),
        acceptedAt: data.acceptedAt,
        competitorId: data.competitorId,
        competitorName: data.competitorName,
        proposedDebitFee: data.proposedDebitFee,
        proposedCreditFee: data.proposedCreditFee,
        competitorDebitFee: data.competitorDebitFee,
        competitorCreditFee: data.competitorCreditFee,
        creditDiscountFeePercentage: data.creditDiscountFeePercentage,
        debitDiscountFeePercentage: data.debitDiscountFeePercentage,
        industryName: data.industryName,
        clientCellphone: data.clientCellphone,
        clientCpfOrCnpj: data.clientCpfOrCnpj,
        clientEmail: data.clientEmail,
      );
    }

    return (await select(proposalsTable).get())
        .map<Proposal>(convertDataModel)
        .toList();
  }
}
