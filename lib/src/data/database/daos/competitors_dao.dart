import 'package:moor/moor.dart';
import 'package:simulator/src/data/database/app_database.dart';
import 'package:simulator/src/data/database/tables/competitors_table.dart';
import 'package:simulator/src/models/company.dart';

part 'competitors_dao.g.dart';

@UseDao(tables: [CompetitorsTable])
class CompetitorsDao extends DatabaseAccessor<AppDatabase>
    with _$CompetitorsDaoMixin {
  CompetitorsDao(AppDatabase db) : super(db);

  Future<void> insertCompanies(List<Company> companies) async {
    final dbCompanies = companies
        .map<DbCompany>((company) => DbCompany.fromJson(company.toJson()))
        .toList();

    await into(competitorsTable).insertAll(dbCompanies, orReplace: true);
  }

  Future<List<Company>> getAllCompanies() async {
    return (await select(competitorsTable).get())
        .map((dbCompany) => Company.fromJson(dbCompany.toJson()))
        .toList();
  }
}
