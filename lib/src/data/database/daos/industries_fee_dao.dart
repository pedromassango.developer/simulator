import 'package:moor/moor.dart';
import 'package:simulator/src/data/database/app_database.dart';
import 'package:simulator/src/data/database/tables/industries_fee_table.dart';
import 'package:simulator/src/models/industry_fee.dart';

part 'industries_fee_dao.g.dart';

@UseDao(tables: [IndustriesFeeTable])
class IndustriesFeeDao extends DatabaseAccessor<AppDatabase>
    with _$IndustriesFeeDaoMixin {

  IndustriesFeeDao(AppDatabase db) : super(db);

  Future<void> insertIndustriesFee(List<IndustryFee> industriesFee) async {
    final dbData = industriesFee.map<DbIndustryFee>((fee) =>
        DbIndustryFee.fromJson(fee.toJson())).toList();

    await delete(industriesFeeTable).go();
    await into(industriesFeeTable).insertAll(dbData);
  }

  Future<IndustryFee> getIndustryFee(String industryId) async {
    final data = await (select(industriesFeeTable)
      ..where((filter) => filter.industryId.equals(industryId))
      ..limit(1)
    ).getSingle();

    if(data != null) {
      return IndustryFee.fromJson(data.toJson());
    }
    return null;
  }
}
