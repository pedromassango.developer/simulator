import 'package:moor/moor.dart';
import 'package:simulator/src/data/database/app_database.dart';
import 'package:simulator/src/data/database/tables/industries_table.dart';
import 'package:simulator/src/models/industry.dart';

part 'industries_dao.g.dart';

@UseDao(tables: [IndustriesTable])
class IndustriesDao extends DatabaseAccessor<AppDatabase>
    with _$IndustriesDaoMixin {
  IndustriesDao(AppDatabase db) : super(db);

  Future<void> insertIndustries(List<Industry> industries) async {
    final dbIndustries = industries
        .map<DbIndustry>((industry) => DbIndustry.fromJson(industry.toJson()))
        .toList();

    await into(industriesTable).insertAll(dbIndustries, orReplace: true);
  }

  Future<List<Industry>> getAllIndustries() async {
    return (await select(industriesTable).get())
        .map<Industry>((dbIndustry) => Industry.fromJson(dbIndustry.toJson()))
        .toList();
  }
}
