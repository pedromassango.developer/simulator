import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:simulator/src/data/database/daos/proposals_dao.dart';
import 'package:simulator/src/data/database/daos/competitors_dao.dart';
import 'package:simulator/src/data/database/daos/industries_dao.dart';
import 'package:simulator/src/data/database/daos/industries_fee_dao.dart';
import 'package:simulator/src/data/database/tables/proposals_table.dart';
import 'package:simulator/src/data/database/tables/competitors_table.dart';
import 'package:simulator/src/data/database/tables/industries_fee_table.dart';
import 'package:simulator/src/data/database/tables/industries_table.dart';

part 'app_database.g.dart';

@UseMoor(tables: [
  IndustriesTable,
  CompetitorsTable,
  IndustriesFeeTable,
  ProposalsTable
], daos: [
  IndustriesDao,
  CompetitorsDao,
  IndustriesFeeDao,
  ProposalsDao
])
class AppDatabase extends _$AppDatabase {
  AppDatabase._()
      : super(FlutterQueryExecutor.inDatabaseFolder(path: 'simulator.sqlite'));

  @override
  int get schemaVersion => 5;

  @override
  MigrationStrategy get migration => MigrationStrategy(
      onCreate: (migrator) => migrator.createAllTables(),
      onUpgrade: (migrator, from, to) async {
        if (from == 2) {
          await migrator.createTable(competitorsTable);
        } else if (from == 3) {
          await migrator.createTable(industriesFeeTable);
        } else if(from == 4) {
          await migrator.createTable(proposalsTable);
        } else if(from == 5) {
          await migrator.addColumn(proposalsTable, proposalsTable.status);
        }
      });

  static AppDatabase instance;

  static AppDatabase getInstance() {
    if (instance == null) {
      instance = AppDatabase._();
    }
    return instance;
  }
}
