import 'package:moor/moor.dart';

@DataClassName("DbProposal")
class ProposalsTable extends Table {
  TextColumn get id => text()();
  TextColumn get status => text()();

  TextColumn get competitorId => text()();
  TextColumn get competitorName => text()();

  RealColumn get competitorDebitFee => real()();
  RealColumn get competitorCreditFee => real()();

  RealColumn get proposedDebitFee => real()();
  RealColumn get proposedCreditFee => real()();

  TextColumn get industryName => text()();
  RealColumn get creditDiscountFeePercentage => real()();
  RealColumn get debitDiscountFeePercentage => real()();

  TextColumn get clientCpfOrCnpj => text()();
  TextColumn get clientCellphone => text()();
  TextColumn get clientEmail => text()();

  DateTimeColumn get acceptedAt => dateTime()();

  @override
  Set<Column> get primaryKey => {id};
}
