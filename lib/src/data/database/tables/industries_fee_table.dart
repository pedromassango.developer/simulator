import 'package:moor/moor.dart';

@DataClassName("DbIndustryFee")
class IndustriesFeeTable extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get industryId => text()();

  RealColumn get minDebitFee => real()();

  RealColumn get minCreditFee => real()();
}
