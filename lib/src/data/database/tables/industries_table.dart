import 'package:moor/moor.dart';

@DataClassName("DbIndustry")
class IndustriesTable extends Table {
  TextColumn get id => text()();

  TextColumn get name => text()();

  @override
  Set<Column> get primaryKey => {id};
}