import 'package:moor/moor.dart';

@DataClassName("DbCompany")
class CompetitorsTable extends Table {
  TextColumn get id => text()();
  TextColumn get name => text()();

  @override
  Set<Column> get primaryKey => {id};
}