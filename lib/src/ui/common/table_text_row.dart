import 'package:flutter/material.dart';

class TableTextRow extends StatelessWidget {
  final String title;
  final Color textColor;

  const TableTextRow(this.title, [this.textColor = Colors.black])
      : assert(title != null),
        assert(textColor != null);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(7.5),
      child: Center(
        child: Text(
          title,
          style: TextStyle(fontSize: 14, color: textColor),
        ),
      ),
    );
  }
}