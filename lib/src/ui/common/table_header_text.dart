import 'package:flutter/material.dart';

class TableTitleText extends StatelessWidget {
  final String title;
  final double fontSize;

  const TableTitleText(this.title, [this.fontSize = 14]) : assert(title != null);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Center(
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline.copyWith(
              fontSize: fontSize,
              fontWeight: FontWeight.bold
          ),
        ),
      ),
    );
  }
}