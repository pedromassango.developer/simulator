import 'package:flutter/material.dart';

class  AppColors {
  static const Color appBarColor = Colors.deepOrange;
  static const Color buttonBackgroundColor = Color(0xff585858);
}