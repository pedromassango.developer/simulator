import 'package:flutter_test/flutter_test.dart';
import 'package:simulator/src/models/fee_info.dart';
import 'package:simulator/src/models/fee_type.dart';
import 'package:simulator/src/models/industry_fee.dart';

void main() {
  IndustryFee industryFee;

  setUpAll(() {
    industryFee = IndustryFee(
        industryId: '1',
        minDebitFee: 2.0,
        minCreditFee: 3.0
    );
  });

  test(("calculate credit percentage of discount for a client"), () {
    final creditFee = FeeInfo(
        type: FeeType.credit,
        competitorFee: 4.0,
        proposedDiscount: 10
    );

    final discount = creditFee.discountPercentage();
    expect(discount, equals(3.6));
    expect(industryFee.isCreditFeeAllowed(creditFee.discountPercentage()),
        equals(true));
  });

  test(("calculate debit percentage of discount for a client"), () {
    final debitFee = FeeInfo(
        type: FeeType.debit,
        competitorFee: 3.5,
        proposedDiscount: 50
    );

    final discount = debitFee.discountPercentage();
    expect(discount, equals(1.75));
    expect(industryFee.isDebitFeeAllowed(debitFee.discountPercentage()),
        equals(false));
  });
}